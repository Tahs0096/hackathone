#!/bin/bash
PN=`basename "$0"`			# program name
VER='1.3'

: ${AWK:=nawk}

# Find mail spool directories by searching in frequent locations
for MailDir in /usr/spool/mail /var/mail /var/spool/mail
do [ -d "$MailDir" ] && break
done

usage () {
    echo >&2 "$PN - show top 10 directory users, $VER (stv '94)
usage: $PN [directory ...]

If no directory is specified, $MailDir is the default."
    exit 1
}

[ $# -gt 0 -a X"$1" = X"-h" ] && usage

# set the default directory
[ $# -lt 1 ] && set -- $MailDir

ls -l "$@" |
    $AWK '
    	!DetectedColumns && $NF >= 5 {
	    # Set defaults
	    Col["user"] = 3
	    Col["size"] = 5

	    # Examples of a line:
	    # -rw-rw---- 1 heiner  mail  86232778 Apr 23 00:38 heiner
	    # -rw-------  1 andrea     286282 Oct 21 11:24 andrea

	    if ($1 ~ /........../  && $2 ~ /^[0-9][0-9]*$/ ) {
		Col["user"] = 3
	    }
	    for (i=3; i<=5; ++i) {
		# Next number is file size
		if ($i ~ /^[0-9][0-9]*$/)
		    Col["size"] = i
	    }
	    DetectedColumns = 1
	}

	(NF >= 5) {
	    user = $(Col["user"])
	    size = $(Col["size"])
	    Usage [user] += size	# used bytes, username is index
	    Count [user]++
	    TotalBytes += size
	    TotalFiles++
	}
	END {
	    for ( user in Usage ) {
		TotalUsers++;
		if ( !TotalBytes ) TotalBytes = 1
		printf "%-15s %12d	(%d files, %2d%%)\n", \
		    user, Usage [user], Count [user], \
		    Usage [user] * 100 / TotalBytes
	    }
	    #printf "%d users, %d files, %d KB\n", \
	    #	TotalUsers, TotalFiles, TotalBytes/1024
	}
    ' | sort -nr +1 | head
exit 0
