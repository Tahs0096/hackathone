#!/bin/bash
RAYGUN_AUTH_TOKEN="cn5dRzxkMe3uGoaWVH5CZM7eMW0DCC2b"
RAYGUN_API_KEY="uUQ5KaCXCHcjBIO4MkYAGw"
DEPLOYMENT_VERSION="2.190.2"
DEPLOYED_BY="AYUSH"
EMAIL_ADDRESS="abc123@gmail.com"
DEPLOYMENT_NOTES="This is for test"
GIT_HASH="807ff041af95f3bd9bab1f3edf2c98476866a1e4"
HELP=0

while getopts "t:a:v:n:e:g:h" opt; do
        case $opt in
        t)
                RAYGUN_AUTH_TOKEN=$OPTARG
                ;;
        a)
                RAYGUN_API_KEY=$OPTARG
                ;;
        v)
                DEPLOYMENT_VERSION=$OPTARG
                ;;
        n)
                DEPLOYED_BY=$OPTARG
                ;;
        e)
                EMAIL_ADDRESS=$OPTARG
                ;;
    g)
                GIT_HASH=$OPTARG
                ;;
    h)
        HELP=1
        ;;
        esac
done

shift $((OPTIND-1))

if [ $HELP -eq 1 ]
then
cat << EOF
usage: deployment.sh [-h] -v VERSION -t TOKEN -a API_KEY
                      -e EMAIL -n NAME [-g GIT_HASH] NOTES
  h:          show this help
  v VERSION:  version string for this deployment
  t TOKEN:    your Raygun External Auth Token
  a API_KEY:  the API Key for your Raygun Application
  n NAME:     the name of the person who created the deployment
  e EMAIL:    the email address of the person who created the deployment.
              Should be a Raygun users email
  g GIT_HASH: the git commit hash this deployment was built from
  NOTES:      the release notes for this deployment.
              Will be formatted using a Markdown parser
EOF
exit
fi

[ "$1" = "--" ] && shift

if [ "$1" != "" ]
then
    DEPLOYMENT_NOTES=$1
    DEPLOYMENT_NOTES=`echo $DEPLOYMENT_NOTES | sed s/\"/\\\\\\\\\"/g`
fi

echo $RAYGUN_AUTH_TOKEN
echo $RAYGUN_API_KEY
echo $DEPLOYMENT_VERSION
echo $DEPLOYED_BY
echo $EMAIL_ADDRESS
echo $DEPLOYMENT_NOTES
echo $GIT_HASH

url="https://app.raygun.com/deployments?authToken=$RAYGUN_AUTH_TOKEN"
curl -sl $url

read -r -d deployment <<- EOF
{
    apiKey: \"$RAYGUN_API_KEY\",
    version: \"$DEPLOYMENT_VERSION\",
    ownerName: \"$DEPLOYED_BY\",
    emailAddress: \"$EMAIL_ADDRESS\",
    scmIdentifier: \"$GIT_HASH\",
    comment: \"$DEPLOYMENT_NOTES\"
}
EOF

if ! curl -H "Content-Type: application/json" -d "$deployment" -f $url
then
  echo "Could not send deployment details to Raygun"
  exit 1
fi
